#!/usr/bin/env bash
set -e

cmd_usage() {
  cat <<-_EOF
Options:
  help|-h      -  Print usage information
  build|-b     -  Build and install all packages
  rebuild|-r   -  Rebuild all packages
  needed|-n    -  Build packages that are not built
  clean|-c     -  Clean all packages
  uninstall|-u -  Uninstall all packages
  srcinfo|-s   -  Regenerate .SRCINFOs of all packages
Arguments:
  [packages]   -  Provided list of packages to build, if not provided use list
Environment Variables:
  PROJECT_LIST - select list to use
  NO_RESET     - don't reset git branches
  USE_SSH      - use ssh for git
_EOF
  exit 0
}

if [ ! -z "$2" ]; then
  PACKAGES="${@:2}"
else
  PACKAGES=$(cat "${PROJECT_LIST:-projects.list.minimal}")
fi

CURDIR=`pwd`

built() {
  echo "$1" >> "$CURDIR/projects.built"
}

failed() {
  echo "$1" >> "$CURDIR/projects.failed"
}

get_update_src() {
  PKG=$1

  if [ ! -d ${PKG} ]; then
    echo "Cloning PKGBUILD"
    if [ -z "${USE_SSH}" ]; then
        git clone "https://gitlab.manjaro.org/manjaro-arm/packages/community/lomiri-dev/${PKG}.git"
    else
        git clone "ssh://git@gitlab.manjaro.org:22277/manjaro-arm/packages/community/lomiri-dev/${PKG}.git"
    fi
  else
    echo "Updating PKGBUILD"
    cd ${package}
    git fetch
    if [ -z "${NO_RESET}" ]; then
        git reset --hard origin/master
    fi
    cd ..
  fi
}

cmd_clone() {
  for package in $PACKAGES
  do
    get_update_src ${package}
  done
}

cmd_build() {
  sudo test true
  for package in $PACKAGES
  do
    get_update_src ${package}
    cd ${package}
    makepkg -si --noconfirm || failed ${package} && continue
    built ${package}
    echo "----------------------"
    cd ..
  done
}

cmd_rebuild() {
  sudo test true
  for package in $PACKAGES
  do
    get_update_src ${package}
    cd ${package}
    makepkg -sfr --holdver --noconfirm || failed ${package} && continue
    built ${package}
    echo "----------------------"
    cd ..
  done
}

cmd_needed() {
  sudo test true
  for package in $PACKAGES
  do
    echo "Bulding $package"
    if grep -Fxq "$package" "$CURDIR/projects.built"; then
        echo "Skiping $package"
        continue
    fi
    get_update_src ${package}
    cd ${package}
    makepkg -si --needed --noconfirm || failed ${package} && continue
    built ${package}
    echo "----------------------"
    cd ..
  done
}

cmd_clean() {
  sudo test true
  for package in $PACKAGES
  do
    cd ${package}
    makepkg -dCo
    cd ..
  done
}

cmd_uninstall() {
  sudo test true
  for package in $PACKAGES
  do
    yes | sudo pacman -Rdd ${package} || true
    echo "----------------------"
  done
}

cmd_srcinfo() {
  for package in $PACKAGES
  do
    cd ${package}
    makepkg --printsrcinfo > .SRCINFO
    cd ..
  done
}

case "$1" in
  help|-h)      shift; cmd_usage "$@" ;;
  build|-b)     shift; cmd_build "$@" ;;
  rebuild|-r)   shift; cmd_rebuild "$@" ;;
  needed|-n)    shift; cmd_needed "$@" ;;
  clean|-c)     shift; cmd_clean "$@" ;;
  uninstall|-u) shift; cmd_uninstall "$@" ;;
  srcinfo|-s)   shift; cmd_srcinfo "$@" ;;
  clone|-g)     shift; cmd_clone "$@" ;;
  *)                   cmd_usage "$@" ;;
esac

