function ProgressBar {
    let _progress=(${1}*100/${2}*100)/100
    let _done=(${_progress}*4)/10
    let _left=40-$_done

    _done=$(printf "%${_done}s")
    _left=$(printf "%${_left}s")

    printf "\rProgress : [${_done// /#}${_left// /-}] ${_progress}%%"

}


search="${2:-not found}"

if [ -z $1 ]; then
  echo "No folder provided as argument"
  exit
fi

if ! [ -d $1 ]; then
  echo "No such directoty: $1"
  exit
fi

l=`find $1 -type f -name "*.so"`
l2=($l)
le=$((${#l2[@]} + 1))
li=1

ProgressBar $li $le

for i in $l; do
    # To speed it up we search for dynamically linked libs
    if file "$i" | grep -q "dynamically linked"; then
       the_ld=$(ldd "$i" 2> /dev/null | grep "$search")
       if [ -z $the_ld ]; then
          for l in $the_ld; do
             files="$files $(echo $l | awk '{print $1}')"
          done
          mes="$i is missing $files"
          list="$list\n$mes"
          echo "$mes"
       fi
    fi
    li=$(($li + 1))
    ProgressBar $li $le
done
echo " - done"


